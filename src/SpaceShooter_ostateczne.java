import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpaceShooter extends JFrame implements ActionListener, KeyListener {

    private Timer timer;
    private int playerX, playerY, playerSpeed, playerHorizontalSpeed;
    private List<Enemy> enemies;
    private int bulletX, bulletY, bulletSpeed;
    private boolean isShooting;
    private int score;
    private int lives;
    private int blueEnemiesCount;
    private int greenEnemiesCount;
    private int blueEnemiesShot;
    private JLabel scoreLabel;
    private JLabel livesLabel;
    private int difficultyChoice;

    private int[][] playerShipMatrix = {
            {0, 0, 1, 0, 0},
            {0, 1, 1, 1, 0},
            {1, 1, 1, 1, 1},
            {0, 0, 1, 0, 0},
            {0, 1, 1, 1, 0}
    };

    private int[][] blueEnemyMatrix = {
            {0, 1, 1, 1, 0},
            {0, 0, 1, 0, 0},
            {1, 1, 1, 1, 1},
            {0, 1, 1, 1, 0},
            {0, 0, 1, 0, 0}
    };

    private int[][] greenEnemyMatrix = {
            {0, 1, 0},
            {1, 1, 1},
            {0, 1, 0}
    };

    private int blueEnemySpawnDelay = 500; // Opóźnienie w milisekundach między niebieskimi przeszkodami
    private int currentBlueEnemySpawnDelay = 0;

    public SpaceShooter(int difficultyChoice) {
        this.difficultyChoice = difficultyChoice;
        setTitle("Space Shooter");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        playerX = 400;
        playerY = 500;
        playerSpeed = 5;
        playerHorizontalSpeed = 15;

        enemies = new ArrayList<>();

        if (difficultyChoice == 0) {  // Easy level
            initializeEasyEnemies();
        } else {  // Hard level
            blueEnemiesCount = 3;
            greenEnemiesCount = 3;
            initializeEnemies();
        }

        bulletX = -1;
        bulletY = -1;
        bulletSpeed = 10;
        isShooting = false;

        score = 0;
        lives = 3;
        blueEnemiesShot = 0;

        timer = new Timer(10, this);
        timer.start();

        addKeyListener(this);
        setFocusable(true);

        SpacePanel spacePanel = new SpacePanel();
        add(spacePanel);

        scoreLabel = new JLabel("Score: " + score);
        scoreLabel.setForeground(Color.WHITE);
        scoreLabel.setFont(new Font("Arial", Font.BOLD, 16));
        spacePanel.add(scoreLabel);

        livesLabel = new JLabel("Lives: " + lives);
        livesLabel.setForeground(Color.WHITE);
        livesLabel.setFont(new Font("Arial", Font.BOLD, 16));
        spacePanel.add(livesLabel);
    }

    private void initializeEasyEnemies() {
        for (int i = 0; i < 5; i++) {
            Random rand = new Random();
            int x = rand.nextInt(750);
            int y = rand.nextInt(300);
            int[][] easyEnemyMatrix = {
                    {0, 1, 0},
                    {1, 1, 1},
                    {0, 1, 0}
            };
            enemies.add(new Enemy(x, y, Color.GREEN, 1, easyEnemyMatrix));
        }
    }

    private void initializeEnemies() {
        for (int i = 0; i < blueEnemiesCount; i++) {
            Random rand = new Random();
            int x = rand.nextInt(750);
            int y = rand.nextInt(300);
            enemies.add(new Enemy(x, y, Color.BLUE, 2, blueEnemyMatrix));
        }

        // Hard - blue
        if (difficultyChoice == 1) {
            for (int i = 0; i < 60; i++) {
                currentBlueEnemySpawnDelay = i * blueEnemySpawnDelay;
                scheduleBlueEnemySpawn();
            }
        }

        for (int i = 0; i < greenEnemiesCount; i++) {
            Random rand = new Random();
            int x = rand.nextInt(750);
            int y = rand.nextInt(300);
            enemies.add(new Enemy(x, y, Color.GREEN, 2, greenEnemyMatrix));
        }
    }

    private void scheduleBlueEnemySpawn() {
        Timer spawnTimer = new Timer(currentBlueEnemySpawnDelay, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random rand = new Random();
                int x = rand.nextInt(750);
                int y = rand.nextInt(2000) - 1000;
                enemies.add(new Enemy(x, y, Color.BLUE, 2, blueEnemyMatrix));
            }
        });

        spawnTimer.setRepeats(false);
        spawnTimer.start();
    }

    public void actionPerformed(ActionEvent e) {
        update();
        repaint();
    }

    public void update() {
        if (isShooting) {
            bulletY -= bulletSpeed;
            if (bulletY < 0) {
                isShooting = false;
                bulletY = -1;
                if (lives > 0) {
                    lives--;
                    livesLabel.setText("Lives: " + lives);
                    if (lives == 0) {
                        endGame();
                    }
                }
            }
        }

        for (Enemy enemy : enemies) {
            enemy.move();
            if (enemy.getColor() == Color.GREEN && enemy.getY() > getHeight()) {
                if (lives > 0) {
                    lives--;
                    livesLabel.setText("Lives: " + lives);
                    if (lives == 0) {
                        endGame();
                    }
                }
                enemy.resetPosition();
            }
        }

        checkCollisions();
        scoreLabel.setText("Score: " + score);

        if (score == 15 && difficultyChoice == 1) {
            winGame();
        }
    }

    private void checkCollisions() {
        Rectangle bulletRect = new Rectangle(bulletX, bulletY, 5, 10);

        for (Enemy enemy : enemies) {
            Rectangle enemyRect = new Rectangle(enemy.getX(), enemy.getY(), 30, 30);

            if (bulletRect.intersects(enemyRect)) {
                isShooting = false;
                bulletY = -1;
                if (enemy.getColor() == Color.GREEN) {
                    score++;
                } else if (enemy.getColor() == Color.BLUE) {
                    score--;
                    blueEnemiesShot++;
                }
                enemy.resetPosition();
            }
        }
    }

    private void endGame() {
        timer.stop();
        JOptionPane.showMessageDialog(this, "Game Over!\nYour score: " + score +
                "\nBlue enemies shot: " + blueEnemiesShot, "Game Over", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }

    private void winGame() {
        timer.stop();
        JOptionPane.showMessageDialog(this, "Congratulations!\nYou won the game!", "Victory", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }

    private class Enemy {
        private int x, y;
        private int speed;
        private Color color;
        private int[][] enemyMatrix;

        public Enemy(int x, int y, Color color, int speed, int[][] enemyMatrix) {
            this.x = x;
            this.y = y;
            this.speed = speed;
            this.color = color;
            this.enemyMatrix = enemyMatrix;
        }

        public void move() {
            y += speed;
        }

        public void resetPosition() {
            Random rand = new Random();
            x = rand.nextInt(750);
            y = rand.nextInt(300) - 400;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Color getColor() {
            return color;
        }

        public void draw(Graphics2D g) {
            g.setColor(color);

            for (int i = 0; i < enemyMatrix.length; i++) {
                for (int j = 0; j < enemyMatrix[i].length; j++) {
                    if (enemyMatrix[i][j] == 1) {
                        g.fillRect(x + j * 10, y + i * 10, 10, 10);
                    }
                }
            }
        }
    }

    private class SpacePanel extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;


            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            draw(g2d);
        }

        public void draw(Graphics2D g) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, getWidth(), getHeight());

            drawPlayerShip(g);

            if (isShooting) {
                g.setColor(Color.YELLOW);
                g.fillRect(bulletX, bulletY, 5, 10);
            }

            for (Enemy enemy : enemies) {
                enemy.draw(g);
            }
        }

        private void drawPlayerShip(Graphics2D g) {
            g.setColor(Color.WHITE);

            for (int i = 0; i < playerShipMatrix.length; i++) {
                for (int j = 0; j < playerShipMatrix[i].length; j++) {
                    if (playerShipMatrix[i][j] == 1) {
                        g.fillRect(playerX + j * 10, playerY + i * 10, 10, 10);
                    }
                }
            }
        }
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT && playerX > 0) {
            playerX -= playerHorizontalSpeed;
        } else if (key == KeyEvent.VK_RIGHT && playerX < getWidth() - 50) {
            playerX += playerHorizontalSpeed;
        } else if (key == KeyEvent.VK_SPACE && !isShooting) {
            isShooting = true;
            bulletX = playerX + 22;
            bulletY = playerY;
        }
    }

    public void keyReleased(KeyEvent e) {
        // Not needed for this simple example
    }

    public void keyTyped(KeyEvent e) {
        // Not needed for this simple example
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                String[] difficultyOptions = {"Easy", "Hard"};
                int difficultyChoice = JOptionPane.showOptionDialog(
                        null,
                        "Choose Difficulty",
                        "Difficulty",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        difficultyOptions,
                        difficultyOptions[0]
                );

                new SpaceShooter(difficultyChoice).setVisible(true);
            }
        });
    }
}
